﻿using UnityEngine;

class AimDownSight : MonoBehaviour
{
    public Transform ADS;
    public Transform hip;
    public Animation rifleAnim;
    public static bool stretch = false;

    public void Start()
    {
        Screen.showCursor = false;
    }

    public void Update()
    {
        if (!TouchPlane.onPlane)
        {
            if (!GameObject.Find("RifleDropped") && MouseLook.canUpdate)
            {
                if (Input.GetMouseButton(1) && !rifleAnim.isPlaying && !Input.GetKey(KeyCode.LeftShift))
                {
                    transform.position = ADS.position;
                    Camera.main.fieldOfView = 35;
                }
                else if (stretch && (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0))
                {
                    Camera.main.fieldOfView = 75;
                }
                else if (!Input.GetMouseButton(1) || rifleAnim.isPlaying || !stretch)
                {
                    transform.position = hip.position;
                    Camera.main.fieldOfView = 60;
                }
            }
        }
    
    }

}
