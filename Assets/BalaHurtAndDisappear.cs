﻿using UnityEngine;

class BalaHurtAndDisappear : MonoBehaviour
{
    public void Start()
    {
        GameObject.Destroy(gameObject, 2);
    }

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            HUDThings.hp -= 20;
            GameObject.Destroy(gameObject);
        }
    }
}