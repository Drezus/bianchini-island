﻿using UnityEngine;

class BalaPlaneHurtAndDisappear : MonoBehaviour
{
    public GameObject balaBoom;

    public void Start()
    {
        GameObject.Destroy(gameObject, 4);
    }

    public void OnTriggerEnter(Collider hit)
    {
            GameObject bB = Instantiate(balaBoom, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
            GameObject.Destroy(gameObject);
    }
}