﻿using UnityEngine;

class BalaTurretHurtAndDisappear : MonoBehaviour
{
    public void Start()
    {
        GameObject.Destroy(gameObject, 10);
    }

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            HUDThings.hp -= 80;
            GameObject.Destroy(gameObject);
        }
    }
}