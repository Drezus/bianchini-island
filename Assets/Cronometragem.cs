﻿using UnityEngine;

class Cronometragem : MonoBehaviour
{
    private float timer = 600;

    public void Update()
    {
        if (!MusicManager.isCredits)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;

                //Minutos: Timer/60
                //Segundos: Timer mod (%) 60

                int minutes = (int)(timer / 60);
                int seconds = (int)(timer % 60);

                //0 = uma casa depois da vírgula  ---  1 = duas casas depois da vírgula  --- :00 = quantas casas irão aparecer
                string s = string.Format("{0:00}:{1:00}", minutes, seconds);

                guiText.text = s;
            }
            else
            {
                Application.LoadLevel(0);
            }
        }
    }
}