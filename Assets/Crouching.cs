﻿using UnityEngine;

class Crouching : MonoBehaviour
{
    public Transform MainPos;
    public Transform CrouchPos;

    public void Update()
    {
        if (!GameObject.Find("RifleDropped"))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                GameObject.Find("Main Camera").transform.position = CrouchPos.position;
            }
            else
            {
                GameObject.Find("Main Camera").transform.position = MainPos.position;
            }
        }
    }
}