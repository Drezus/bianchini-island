﻿using UnityEngine;

[RequireComponent(typeof (CharacterController))]
public class FPSWalker : MonoBehaviour
{
    public float speed = 2F;
    public float jumpSpeed = 8F;
    public float gravity = 20F;

    private bool applyGravity = true;
    private Vector3 moveDirection = Vector3.zero;
    private bool grounded = false;

    public void FixedUpdate()
    {
        if (CharacterMotor.canControl)
        {

            if (grounded)
            {
                // We are grounded, so recalculate movedirection directly from axes
                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                moveDirection = transform.TransformDirection(moveDirection);

                if (!GameObject.Find("RifleDropped"))
                {
                    if (Input.GetMouseButton(1) && !RifleReload.reloading)
                    {
                        moveDirection *= -speed;
                        AimDownSight.stretch = false;
                    }
                    //else if (Input.GetKey(KeyCode.LeftControl))
                    //{
                    //    moveDirection *= -speed;
                    //    AimDownSight.stretch = false;
                    //}
                    else if (Input.GetKey(KeyCode.LeftShift) && !RifleReload.reloading && !Input.GetMouseButton(0))
                    {
                        moveDirection *= speed * 4;
                        AimDownSight.stretch = true;
                    }
                    else if (!Input.GetMouseButton(1) && !Input.GetKey(KeyCode.LeftShift) || RifleReload.reloading || Input.GetMouseButton(0))
                    {
                        moveDirection *= speed;
                        AimDownSight.stretch = false;
                    }
                }

                if (Input.GetButton("Jump"))
                {
                    moveDirection.y = jumpSpeed;
                    ResetParent();
                }
            }

            // Apply gravity
            if (applyGravity) moveDirection.y -= gravity * Time.deltaTime;

            CharacterController controller = GetComponent<CharacterController>();
            CollisionFlags flags = controller.Move(moveDirection * Time.deltaTime);

            grounded = (flags & CollisionFlags.CollidedBelow) != 0;
            if (!applyGravity)
            {
                grounded = true;
                transform.position = new Vector3(transform.position.x, transform.parent.position.y + 0.84F, transform.position.z);
            }
        }
    }

    private void ResetParent()
    {
        Quaternion rot = transform.rotation;
        transform.parent = null;
        transform.rotation = rot;
        applyGravity = true;
    }

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.tag == "plat")
        {
            transform.parent = hit.transform;
            applyGravity = false;
        }
    }

    public void OnTriggerExit(Collider hit)
    {
        if (hit.tag == "plataforma")
        {
            ResetParent();
        }
    }
}
