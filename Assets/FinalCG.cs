﻿using UnityEngine;

class FinalCG : MonoBehaviour
{
    public static bool start = false;
    public float timer;
    public GameObject detonator;
    //public Transform bigExplosion;
    public AudioClip bigBoomSound;
    public Transform place1;
    public Transform place2;
    public Transform place3;
    public Transform place4;
    public Transform place5;

    public Color white = new Color (225,225,225,1);
    public Color transp = new Color(225, 225, 225, 0);

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "bala-plane")
        {
            Destroy(hit);
            start = true;
        }
    }

    public void Update()
    {
        if (start)
        {
            timer += Time.deltaTime;

            if (timer > 1 && timer < 1.05)
            {
                GameObject.Find("First Person Controller").GetComponent<AudioSource>().Stop();
                GameObject.Find("Main Camera").camera.enabled = false;
                GameObject.Find("PlaneCamera").camera.enabled = false;
                GameObject.Find("TotemCamera").camera.enabled = true;

                CharacterMotor.canControl = false;
                MouseLook.canUpdate = false;
            }
            if (timer > 2 && timer < 2.05)
            {
                GameObject finalBoom = Instantiate(detonator, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
            }
            if (timer > 4 && timer < 4.05)
            {
                GameObject.Find("TotemCamera").camera.enabled = false;
                GameObject.Find("IslandCamera").camera.enabled = true;

                MusicManager.isCredits = true;
            }
            if (timer > 6 && timer < 6.05)
            {
                GameObject finalBoom = Instantiate(detonator, place1.transform.position, place1.transform.rotation) as GameObject;
            }
            if (timer > 8 && timer < 8.05)
            {
                GameObject finalBoom = Instantiate(detonator, place2.transform.position, place2.transform.rotation) as GameObject;
            }
            if (timer > 11 && timer < 11.05)
            {
                GameObject finalBoom = Instantiate(detonator, place3.transform.position, place3.transform.rotation) as GameObject;
                GameObject.Find("IslandCamera").GetComponent<ScreenFade>().SetScreenOverlayColor(transp);
                GameObject.Find("IslandCamera").GetComponent<ScreenFade>().StartFade(white, 12);
            }
            if (timer > 13 && timer < 13.05)
            {
                GameObject finalBoom = Instantiate(detonator, place4.transform.position, place4.transform.rotation) as GameObject;
            }
            if (timer > 14 && timer < 14.05)
            {
                GameObject finalBoom = Instantiate(detonator, place5.transform.position, place5.transform.rotation) as GameObject;
            }
            if (timer > 22.5 && timer < 22.55)
            {
                GameObject.Find("IslandCamera").camera.enabled = false;
                GameObject.Find("CreditsCamera").camera.enabled = true;
                GameObject.Find("GUI Cronometro").GetComponent<GUIText>().enabled = false;
                GameObject.Find("GUI Ammo").GetComponent<GUIText>().enabled = false;
                GameObject.Find("Crosshair").GetComponent<GUITexture>().enabled = false;
                GameObject.Find("Minimap Camera").GetComponent<Camera>().enabled = false;

                GameObject.Find("GUI Credits").GetComponent<GUITexture>().enabled = true;

                GameObject.Find("IslandCamera").GetComponent<ScreenFade>().SetScreenOverlayColor(white);
                GameObject.Find("IslandCamera").GetComponent<ScreenFade>().StartFade(transp, 2);
            }
        }
    }
}
