﻿using UnityEngine;

class GetAmmo : MonoBehaviour
{
    public AudioClip pickupAmmo;

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            if (RifleReload.totalBullets < RifleReload.maxCapacity)
            {
                GameObject.Find("First Person Controller").GetComponent<AudioSource>().PlayOneShot(pickupAmmo);
                RifleReload.totalBullets += RifleReload.clipSize * 2;
                GameObject.Destroy(gameObject);

                if (RifleReload.totalBullets > RifleReload.maxCapacity)
                {
                    RifleReload.totalBullets = RifleReload.maxCapacity;
                }
            }
        }
    }
}