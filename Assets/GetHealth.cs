﻿using UnityEngine;

class GetHealth : MonoBehaviour
{
    public AudioClip pickupHP;

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            if (HUDThings.hp < HUDThings.maxHP)
            {
                GameObject.Find("First Person Controller").GetComponent<AudioSource>().PlayOneShot(pickupHP);
                HUDThings.hp += 75;
                GameObject.Destroy(gameObject);

                if (HUDThings.hp > HUDThings.maxHP)
                {
                    HUDThings.hp = HUDThings.maxHP;
                }
            }
        }
    }
}