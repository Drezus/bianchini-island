﻿using UnityEngine;

class GetKei : MonoBehaviour
{
    public AudioClip Kei1;
    public AudioClip Kei2;
    public AudioClip Kei3;
    public AudioClip Kei4;

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {

            if (GameObject.Find("First Person Controller").GetComponent<HUDThings>().keisOnGUI == 0)
            {
                GameObject.Find("First Person Controller").GetComponent<AudioSource>().PlayOneShot(Kei1);
            }
            else if (GameObject.Find("First Person Controller").GetComponent<HUDThings>().keisOnGUI == 1)
            {
                GameObject.Find("First Person Controller").GetComponent<AudioSource>().PlayOneShot(Kei2);
            }
            else if (GameObject.Find("First Person Controller").GetComponent<HUDThings>().keisOnGUI == 2)
            {
                GameObject.Find("First Person Controller").GetComponent<AudioSource>().PlayOneShot(Kei3);
            }
            else if (GameObject.Find("First Person Controller").GetComponent<HUDThings>().keisOnGUI == 3)
            {
                GameObject.Find("First Person Controller").GetComponent<AudioSource>().PlayOneShot(Kei4);
                HUDThings.startCam = true;
            } 


            if (GameObject.Find("First Person Controller").GetComponent<HUDThings>().keisOnGUI < 4)
            {
                GameObject.Find("First Person Controller").GetComponent<HUDThings>().keisOnGUI++;
                GameObject.Destroy(gameObject);
            }

        }
    }
}