﻿using UnityEngine;

class GetMasterKei : MonoBehaviour
{
    public Transform player;
    public Transform respawnPoint;

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            MusicManager.labCleared = true;
            player.position = respawnPoint.position;
            player.rotation = respawnPoint.rotation;
            LabTrigger.inLab = false;
            GameObject.Destroy(gameObject);
        }
    }
}