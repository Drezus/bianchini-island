﻿using UnityEngine;

class GetRifle : MonoBehaviour
{
    public Animation rifle;
    public AudioClip pickupWeapon;

    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "rifle")
        {
            GameObject.Find("Rifle").GetComponent<AudioSource>().PlayOneShot(pickupWeapon);
            GameObject.Destroy(hit.gameObject);
            rifle.Play("rifle_equip");
        }
    }

}
