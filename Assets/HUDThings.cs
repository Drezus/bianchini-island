﻿using UnityEngine;

class HUDThings : MonoBehaviour
{
    public Texture2D kei;
    public Texture2D lifeBar;
    public static int maxHP = 300;
    public static int hp;
    //private bool showButton = true;
    public int keisOnGUI = 0;
    public Transform player;
    public Transform respawnPoint;
    public Transform bossRespawnPoint;

    public AudioClip AllKeis;
    public float timer;
    public float timerSun;
    public static bool startCam = false;
    public static bool startSunCG = false;
    public GameObject blueSun;
    public static bool animateLab = false;


    public void Start()
    {
        hp = maxHP;

        GameObject.Find("TotemCamera").camera.enabled = false;
        GameObject.Find("SunCamera1").camera.enabled = false;
        GameObject.Find("SunCamera2").camera.enabled = false;
        GameObject.Find("LabCamera").camera.enabled = false;
        GameObject.Find("PlaneCamera").camera.enabled = false;
        GameObject.Find("CreditsCamera").camera.enabled = false;
        GameObject.Find("IslandCamera").camera.enabled = false;
        CharacterMotor.canControl = true;
        MouseLook.canUpdate = true;
    }

    public void OnGUI()
    {
        if (!TouchPlane.onPlane)
        {
            for (int i = 0; i < keisOnGUI; i++)
            {
                GUI.DrawTexture(new Rect(10 + i * (kei.width + 10), 10, kei.width, kei.height), kei);
            }

            GUI.BeginGroup(new Rect(10, Screen.height * 0.9f + 30, hp, 20));
            GUI.DrawTexture(new Rect(0, 0, maxHP, 50), lifeBar);
            GUI.EndGroup();

            if (hp <= 0)
            {
                //Respawnar no Checkpoint correto
                if (!LabTrigger.inLab)
                {
                    player.position = respawnPoint.position;
                    player.rotation = respawnPoint.rotation;
                }
                else
                {
                    player.position = bossRespawnPoint.position;
                    player.rotation = bossRespawnPoint.rotation;
                }


                hp = maxHP;
                if (RifleReload.totalBullets == 0) RifleReload.totalBullets += RifleReload.clipSize * 2;
            }
        }
    }


    //Câmeras e Cheats
    public void Update()
    {
        if (startCam)
        {
            timer += Time.deltaTime;

            if (timer > 1 && timer < 1.05)
            {
                GameObject.Find("Main Camera").camera.enabled = false;
                GameObject.Find("TotemCamera").camera.enabled = true;

                CharacterMotor.canControl = false;
                MouseLook.canUpdate = false;
                GetComponent<AudioSource>().PlayOneShot(AllKeis);
            }

            if (timer > 5 && timer < 5.05)
            {
                GameObject.Find("TotemCamera").camera.enabled = false;
                GameObject.Find("Main Camera").camera.enabled = true;

                CharacterMotor.canControl = true;
                MouseLook.canUpdate = true;
            }

            if (timer > 6)
            {
                timer = 6;
                startCam = false;
            }
        }

        if (startSunCG)
        {
            timerSun += Time.deltaTime;

            if (blueSun)
            {
                GameObject.Find("SunCamera1").transform.LookAt(blueSun.transform.position);
                GameObject.Find("SunCamera2").transform.LookAt(blueSun.transform.position);

                if (timerSun > 0 && timerSun < 0.05)
                {
                    GameObject.Find("Main Camera").camera.enabled = false;
                    GameObject.Find("SunCamera1").camera.enabled = true;

                    CharacterMotor.canControl = false;
                    MouseLook.canUpdate = false;
                }
                if (timerSun > 5 && timerSun < 5.05)
                {
                    GameObject.Find("SunCamera1").camera.enabled = false;
                    GameObject.Find("SunCamera2").camera.enabled = true;
                }
                
            }
            else
            {
                if (GameObject.Find("SunCamera2").camera.enabled == true)
                {
                    GetComponent<AudioSource>().PlayOneShot(AllKeis);
                    animateLab = true;
                    GameObject.Find("SunCamera2").camera.enabled = false;
                    GameObject.Find("LabCamera").camera.enabled = true;
                }

                if (timerSun > 15 && timerSun < 15.05)
                {
                    GameObject.Find("LabCamera").camera.enabled = false;
                    GameObject.Find("Main Camera").camera.enabled = true;

                    GameObject.Destroy(GameObject.Find("InvisWall"));

                    CharacterMotor.canControl = true;
                    MouseLook.canUpdate = true;
                }
                if (timerSun > 16)
                {
                    timerSun = 16;
                    startSunCG = false;
                }
            }
        }


        //4 KEIS CHEAT
        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            GameObject.Find("First Person Controller").GetComponent<HUDThings>().keisOnGUI = 4;
            startCam = true;
            print("4 KEIS CHEAT: Activated");
        }

        //UNCOVER LAB CHEAT
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            GameObject.Destroy(GameObject.Find("LabCover"));
            print("UNCOVER LABYRINTH CHEAT: Activated");
        }

        //GET PLANE KEYS
        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            MusicManager.labCleared = true;
            print("GET PLANE KEY CHEAT: Activated");
        }
    }


}
