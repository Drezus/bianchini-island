﻿using UnityEngine;

class LabExtend : MonoBehaviour
{
    public Animation labAnim;
    public bool extended;
    public bool finishedSFX = false;

    public AudioClip bridgeEnd;

    public void Update()
    {
            if (!labAnim.isPlaying && !extended && HUDThings.animateLab)
            {
                HUDThings.animateLab = false;
                labAnim.CrossFade("lab_extend");
                GetComponent<AudioSource>().Play();
                finishedSFX = false;
                extended = true;
            }
            else if (!labAnim.isPlaying && extended)
            {
                labAnim.Stop();
                if (!finishedSFX)
                {
                    GetComponent<AudioSource>().Stop();
                    GetComponent<AudioSource>().PlayOneShot(bridgeEnd);
                    finishedSFX = true;
                }
            }
    }
}