﻿using UnityEngine;

class LabTrigger : MonoBehaviour
{
    public AudioClip bridgeBoom;
    public static bool inLab;
    public static bool broken;

    public void OnTriggerEnter(Collider obj)
    {
        if (obj.gameObject.tag == "Player")
        {
            //Send Bridge Flying
            GameObject.Find("Ponte").AddComponent<Rigidbody>();
            GameObject.Find("Ponte").GetComponent<Rigidbody>().AddForce(transform.right * -2000);
            GameObject.Find("Ponte").GetComponent<AudioSource>().PlayOneShot(bridgeBoom);
            broken = true;

            //New Checkpoint
            inLab = true;

            //Destroy This
            GameObject.Destroy(gameObject);
        }
    }
}