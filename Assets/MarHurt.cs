﻿using UnityEngine;

class MarHurt : MonoBehaviour
{ 

    public float frequencia = 0.9f;
    public float timer;

    public void Update()
    {
        timer += Time.deltaTime;
    }

    public void OnTriggerStay(Collider hit)
    {
        if (timer >= frequencia)
        {
            timer = 0;
            if (hit.gameObject.tag == "Player")
            {
                HUDThings.hp -= 5;
            }
        }
    }
}