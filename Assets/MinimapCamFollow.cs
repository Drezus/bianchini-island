﻿using UnityEngine;

class MinimapCamFollow : MonoBehaviour
{
    public Transform alvo;
    public float altura;

    public void LateUpdate()
    {
        transform.position = new Vector3(alvo.position.x, altura, alvo.position.z);
        transform.LookAt(alvo.position);
    }

}