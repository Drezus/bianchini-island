﻿using UnityEngine;

class MusicManager : MonoBehaviour
{
    public AudioClip Lab;
    public AudioClip Main;
    public AudioClip Rest;
    public AudioClip Plane;
    public AudioClip Credits;

    public AudioClip MasterKei;

    public static bool startLab;
    public static bool isRest;
    public static bool isOnPlane;
    public static bool isCredits;
    public static bool weaponGet;
    public static bool fadeoutMain;

    public static bool labCleared = false;

    public float fadeSpeed = 0;

    public void Update()
    {
        //Começar Música Principal
        if (!GameObject.Find("RifleDropped") && !weaponGet)
        {
            GetComponent<AudioSource>().Stop();
            audio.clip = Main;
            GetComponent<AudioSource>().Play();
            weaponGet = true;
        }


        //Começar FadeOut da Música Principal
        if (fadeoutMain)
        {
            fadeSpeed = -0.15f;
            fadeoutMain = false;
        }

        //Começar Música de Labirinto
        if (PonteExtend.noSun && LabTrigger.broken && !startLab)
        {
            GetComponent<AudioSource>().Stop();
            audio.clip = Lab;
            GetComponent<AudioSource>().Play();
            startLab = true;
        }

        //Voltar a Música Idle
        if (labCleared)
        {
            if (!isRest)
            {
                GetComponent<AudioSource>().Stop();
                audio.clip = Rest;
                GetComponent<AudioSource>().Play();
                isRest = true;

                for (int i = 1; i == 1; i++)
                {
                    GameObject.Find("First Person Controller").GetComponent<AudioSource>().PlayOneShot(MasterKei);
                }
            }
        }

        if (TouchPlane.onPlane)
        {
            if (!isOnPlane)
            {
                GetComponent<AudioSource>().Stop();
                audio.clip = Plane;
                GetComponent<AudioSource>().Play();
                isOnPlane = true;
            }
        }

        if (isCredits)
        {       
            GetComponent<AudioSource>().Stop();
            audio.clip = Credits;
            GetComponent<AudioSource>().Play();
            isCredits = false;
        }


        //Fade Out da Música Principal
        if (fadeSpeed < 0)
        {
            audio.volume = Mathf.Max(audio.volume + fadeSpeed * Time.deltaTime, 0);
        }
        if (audio.volume == 0)
        {
            GetComponent<AudioSource>().Stop();
            audio.volume = 1;
            fadeSpeed = 0;
        }


    }
}