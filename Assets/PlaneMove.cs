﻿using UnityEngine;

class PlaneMove : MonoBehaviour
{
    public float transVel;
    public GameObject boom;

    public void Update()
    {
        if (TouchPlane.onPlane)
        {
            if (transVel < 0.5f)
            {
                transVel += 0.001f;
            }

            if (transVel > 0.5f)
            {
                transVel -= 0.001f;
            }

            if (Input.GetKey(KeyCode.S))
            {
                if (transVel > 0.2f)
                {
                    transVel -= 0.05f;
                }
            }

            transform.Translate(0, 0, transVel);
            //transform.Rotate(0, 0, rotVel * Time.deltaTime * Input.GetAxis("Horizontal"));
        }
    }

    public void OnCollisionEnter(Collision hit)
    {
        if (TouchPlane.onPlane && hit.gameObject.name != "First Person Controller" && !FinalCG.start)
        {
            ContactPoint contact = hit.contacts[0];
            Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
            Vector3 pos = contact.point;
            GameObject bm = Instantiate(boom, pos, rot) as GameObject;

            GameObject.Find("PlaneCamera").camera.enabled = false;
            GameObject.Find("Main Camera").camera.enabled = true;

            TouchPlane.onPlane = false;

            GameObject.Find("First Person Controller").transform.position = GameObject.Find("CharPos").transform.position;
            GameObject.Find("First Person Controller").transform.rotation = GameObject.Find("Respawn Point").transform.rotation;
            GameObject.Find("Main Camera").GetComponent<MouseLook>().enabled = true;
            GameObject.Find("Minimap Camera").GetComponent<Camera>().orthographicSize = 50;

            Destroy(gameObject);
        }
    }
}