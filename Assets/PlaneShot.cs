﻿using UnityEngine;

class PlaneShot : MonoBehaviour
{
    public Rigidbody bala;
    public Transform boca;

    public void Update()
    {
        if (TouchPlane.onPlane)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Rigidbody b = GameObject.Instantiate(bala, boca.position, boca.rotation) as Rigidbody;
                b.AddForce(transform.forward * 3000);
            }
        }   
    }
}