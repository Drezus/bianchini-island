﻿using UnityEngine;

class PonteExtend : MonoBehaviour
{
    public Animation ponteAnim;
    public Animation totemSun;
    public Transform playerPos;
    public Transform totemPos;
    public float dist = 15;
    public bool extended;
    public bool sunFinished = false;
    public bool sunRest = false;
    public bool sunRan = false;
    public bool finishedSFX = false;
    public static bool noSun = false;

    public AudioClip bridgeEnd;

    public void Update()
    {
        Vector3 distDiff = playerPos.position - totemPos.position;

        if (GameObject.Find("First Person Controller").GetComponent<HUDThings>().keisOnGUI >= 4)
        {
            //Sol Azul
            if (totemSun)
            {
                if (!totemSun.isPlaying && !sunFinished && !sunRest)
                {
                    totemSun.CrossFade("blueSun_start");
                    sunFinished = true;
                }
                else if (!totemSun.isPlaying && sunFinished && !sunRest)
                {
                    totemSun.Stop();
                }
            }



            //Ponte Anim (começa a animar apenas quando estiver perto)
            if (!ponteAnim.isPlaying && !extended && distDiff.magnitude < dist)
            {
                ponteAnim.CrossFade("ponte_4keis");
                GetComponent<AudioSource>().Play();
                finishedSFX = false;
                extended = true;
                MusicManager.fadeoutMain = true;
            }
            else if (!ponteAnim.isPlaying && extended)
            {
                ponteAnim.Stop();
                if (!finishedSFX)
                {
                    GetComponent<AudioSource>().Stop();
                    GetComponent<AudioSource>().PlayOneShot(bridgeEnd);
                    finishedSFX = true;
                }
            }

            //Sol Azul Indicate (começa a animar apenas quando estiver perto)
            if (totemSun)
            {
                if (!totemSun.isPlaying && sunFinished && !sunRest && !sunRan && distDiff.magnitude < dist)
                {
                    totemSun.CrossFade("blueSun_point", 1);
                    sunRest = true;
                    GameObject.Find("TotemSun").GetComponent<AudioSource>().Play();
                }
                else if (!totemSun.isPlaying && sunFinished && sunRest && !sunRan)
                {
                    if (!LabTrigger.inLab)
                    {
                        totemSun.Stop();
                    }
                    else
                    {
                        totemSun.CrossFade("blueSun_run", 1);
                        HUDThings.startSunCG = true;
                        sunRan = true;
                    }
                }
                else if (!totemSun.isPlaying && sunFinished && sunRest && sunRan)
                {
                    GameObject.Find("TotemSun").GetComponent<AudioSource>().Stop();
                    noSun = true;
                    GameObject.Destroy(GameObject.Find("TotemSun"));
                }
            }

        }
    }
}