﻿using UnityEngine;

class Respawner : MonoBehaviour
{
    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            HUDThings.hp = 0;
        }
    }
}
