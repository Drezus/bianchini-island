﻿using UnityEngine;

class RifleReload : MonoBehaviour
{
    public static int clipSize = 60;
    public static int totalBullets = 240;
    public static int maxCapacity = 1200;
    public static int bulletCount = clipSize;
    public static bool reloading = false;
    public static bool outOfAmmo = false;
    public Animation rifleAnim;
    public GUIText GUIAmmo;
    public AudioClip reloadSFX;


    public void Update()
    {
        if (!TouchPlane.onPlane)
        {
            if (Input.GetKeyDown(KeyCode.R) && bulletCount < clipSize && totalBullets > 0 && !reloading)
            {
                reloading = true;
                rifleAnim.CrossFade("rifle_reload", 0.5f);
                GameObject.Find("First Person Controller").GetComponent<AudioSource>().PlayOneShot(reloadSFX);
                //reloading
            }
            else if (bulletCount <= 0 && totalBullets > 0 && !reloading)
            {
                reloading = true;
                rifleAnim.CrossFade("rifle_reload", 0.5f);
                GameObject.Find("First Person Controller").GetComponent<AudioSource>().PlayOneShot(reloadSFX);
                //(Auto)reloading
            }

            else if (bulletCount <= 0 && totalBullets <= 0 && !reloading)
            {
                reloading = false;
                outOfAmmo = true;
                //Out of Ammo
            }


            if (reloading && !rifleAnim.isPlaying)
            {
                if (totalBullets >= (clipSize - bulletCount))
                {
                    totalBullets -= (clipSize - bulletCount);
                    bulletCount = clipSize;
                }
                else
                {
                    bulletCount += totalBullets;
                    totalBullets = 0;
                }

                reloading = false;
                outOfAmmo = false;
                //Reloaded
            }

            if (!GameObject.Find("RifleDropped"))
            {
                GUIAmmo.text = "AMMO: " + bulletCount + "/" + totalBullets;
            }
            else
            {
                GUIAmmo.text = "";
            }
        }

    }

}