﻿using UnityEngine;

class RifleShot : MonoBehaviour
{
    public Rigidbody bala;
    public Transform boca;
    public Animation rifleAnim;
    public float frequencia = 0.07f;
    public float timer;

    public void Update()
    {
        if (!TouchPlane.onPlane)
        {
            timer += Time.deltaTime;

            if (timer >= frequencia)
            {
                timer = 0;

                if (Input.GetMouseButton(0) && !RifleReload.reloading && !RifleReload.outOfAmmo && RifleReload.bulletCount > 0 && !rifleAnim.isPlaying)
                {
                    Rigidbody b = GameObject.Instantiate(bala, boca.position, boca.rotation) as Rigidbody;
                    b.AddForce(transform.forward * 8000);
                    RifleReload.bulletCount--;
                }
            }
        }   
    }
}