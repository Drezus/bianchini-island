﻿using UnityEngine;

class RotateAround : MonoBehaviour
{
    public Transform meioIlha;
    public int speed = 4;

    public void Update()
    {
        transform.RotateAround(meioIlha.position, Vector3.up, speed * Time.deltaTime); 
    }

}