﻿using UnityEngine;

class TouchPlane : MonoBehaviour
{
    public static bool onPlane;

    public void Start()
    {
        GameObject.Find("Plane").GetComponent<MouseLook>().enabled = false;
    }

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Player" && onPlane == false && MusicManager.labCleared)
        {

        GameObject.Find("PlaneCamera").camera.enabled = true;
        GameObject.Find("Main Camera").camera.enabled = false;

        GameObject.Find("Plane").GetComponent<MouseLook>().enabled = true;
        GameObject.Find("Main Camera").GetComponent<MouseLook>().enabled = false;

        GameObject.Find("Plane").GetComponent<MouseLook>().transform.rotation = GameObject.Find("CharPos").transform.rotation;

        GameObject.Find("Minimap Camera").GetComponent<Camera>().orthographicSize = 150;

        GameObject.Find("GUI Ammo").GetComponent<GUIText>().enabled = false;
        GameObject.Find("Crosshair").GetComponent<GUITexture>().enabled = false;

        onPlane = true;

        }
    }

    public void Update()
    {
        if (onPlane)
        {
            GameObject.Find("First Person Controller").transform.position = GameObject.Find("CharPos").transform.position;
            GameObject.Find("First Person Controller").transform.rotation = GameObject.Find("CharPos").transform.rotation;
        }
    }
}