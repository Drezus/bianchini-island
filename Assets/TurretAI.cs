﻿using UnityEngine;

class TurretAI : MonoBehaviour
{
    public Transform alvo;
    public Rigidbody bala;
    public Transform boca;
    public GameObject detonator;
    public AudioClip shot;

    public float minDistance = 80;
    public float frequencia = 1;
    private float timer;
    public int life = 50;

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "bala")
        {
            life--;
        }
        if (hit.gameObject.tag == "bala-plane")
        {
            life = 0;
        }
    }

    public void Update()
    {
        // Fazer a Turret olhar para o Target
        transform.LookAt(alvo.position);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);
        timer += Time.deltaTime;

        // Contar o tempo ate frequency e disparar
        if (timer >= frequencia)
        {
            timer = 0;
            //Novo vetor de posição relativa para verificar a distância
            Vector3 dist = alvo.position - boca.position;

            if (dist.magnitude < minDistance)
            {
                GetComponent<AudioSource>().PlayOneShot(shot);
                Rigidbody b = GameObject.Instantiate(bala, boca.position, boca.rotation) as Rigidbody;
                b.AddForce(dist.normalized * 2000);
            }
        }

        if (life <= 0)
        {
            GameObject boom = GameObject.Instantiate(detonator, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
            GameObject.Destroy(gameObject);
        }
    }

}