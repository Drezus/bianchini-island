﻿using UnityEngine;

class TurretNavioAI : MonoBehaviour
{
    public Transform alvo;
    public Rigidbody bala;
    public Transform boca;
    public GameObject detonator;

    public float frequencia = 5;
    private float timer;
    public int life = 50;

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "bala-plane")
        {
            life = 0;
        }
    }

    public void Update()
    {
        // Fazer a Turret olhar para o Target e rodar 45o pra cima
        transform.LookAt(alvo.position);

        Vector3 rot = transform.eulerAngles;
        rot.x = -45;
        transform.eulerAngles = rot;

        timer += Time.deltaTime;

        // Contar o tempo ate frequency e disparar
        if (timer >= frequencia)
        {
            timer = 0;
            Rigidbody b = GameObject.Instantiate(bala, boca.position, boca.rotation) as Rigidbody;
            b.AddForce(boca.forward * 2500);
        }

        if (life <= 0)
        {
            GameObject boom = GameObject.Instantiate(detonator, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
            GameObject.Destroy(gameObject);
        }
    }

}